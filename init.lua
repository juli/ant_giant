ant_giant = {
	giants = {}
}

minetest.register_chatcommand("ant", {
	func = function(name, param)
		local player = minetest.get_player_by_name(name)
		local props = player:get_properties()
		props.visual_size = {x = 0.1, y = 0.1, z = 0.1}
		props.eye_height = 0.1625
		props.collisionbox = {-0.1, 0.0, -0.1, 0.1, 0.1, 0.1}
		props.selectionbox = {-0.05, 0.0, -0.05, 0.05, 0.1, 0.05}
		player:set_properties(props)
		ant_giant.giants[name] = false
		local physics = player:get_physics_override()
		physics.jump = 1
		physics.speed = 1
		player:set_physics_override(physics)
	end
})

minetest.register_chatcommand("sam", {
	func = function(name, param)
		local player = minetest.get_player_by_name(name)
		local props = player:get_properties()
		props.visual_size = {x = 1, y = 1, z = 1}
		props.eye_height = 1.625
		props.collisionbox = {-0.5, 0.0, -0.5, 0.5, 1, 0.5}
		props.selectionbox = {-0.5, 0.0, -0.5, 0.5, 1, 0.5}
		player:set_properties(props)
		ant_giant.giants[name] = false
		local physics = player:get_physics_override()
		physics.jump = 1
		physics.speed = 1
		player:set_physics_override(physics)
	end
})

minetest.register_chatcommand("giant", {
	func = function(name, param)
		local player = minetest.get_player_by_name(name)
		local props = player:get_properties()
		props.visual_size = {x = 10, y = 10, z = 10}
		props.eye_height = 16.25
		props.collisionbox = {-5, 0.0, -5, 5, 10, 5}
		props.selectionbox = {-5, 0.0, -5, 5, 10, 5}
		props.stepheight = 10
		player:set_properties(props)
		local physics = player:get_physics_override()
		physics.jump = 10
		physics.speed = 10
		player:set_physics_override(physics)
		ant_giant.giants[name] = true
	end
})

minetest.register_on_player_hpchange(function(player, hpchange, reason)
	minetest.log("warn", tostring(hpchange))
	minetest.log("warn", reason.type)
	local name = player:get_player_name()
	if ant_giant.giants[name] == true then
		if reason and reason.type and reason.type == 'fall' then
			if hpchange > -15 then
				return 0
			end
			return hpchange / 10
		end
	end
	return hpchange
end, true)
